﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwaitTasks
{
    public static class AsyncAwait
    {
        public static int Seed { get; set; }

        public static async Task<int[]> CreateArrayAsync()
        {
            return await Task.Run(() =>
            {
                Random random = new(Seed);
                int[] result = new int[10];
                for (int i = 0; i < result.Length; i++)
                {
                    result[i] = random.Next(1, 10);
                }
                return result;
            });
        }

        public static Task<int[]> MultiplyArrayAsync(int[] sourceArray)
        {
            if (sourceArray.Length < 1)
            {
                throw new ArgumentException("Array cannot be empty", nameof(sourceArray));
            }
            return MultiplyArrayInternalAsync(sourceArray);
        }

        private static async Task<int[]> MultiplyArrayInternalAsync(int[] sourceArray)
        {
            return await Task.Run(() => {
                Random random = new(Seed);
                int[] result = new int[sourceArray.Length];
                int randomNumber = random.Next(1, 10);
                for (int i = 0; i < result.Length; i++)
                {
                    result[i] = sourceArray[i] * randomNumber;
                }
                return result;
            });
        }

        public static Task<int[]> SortArrayAsync(int[] sourceArray)
        {
            if (sourceArray.Length < 1)
            {
                throw new ArgumentException("Array cannot be empty", nameof(sourceArray));
            }
            return SortArrayInternalAsync(sourceArray);
        }

        private static async Task<int[]> SortArrayInternalAsync(int[] sourceArray)
        {
            return await Task.Run(() => {
                int[] result = new int[sourceArray.Length];
                Array.Copy(sourceArray, result, sourceArray.Length);
                Array.Sort(result);
                return result;
            });
        }

        public static async Task AsyncTask()
        {
            var array = await CreateArrayAsync();
            var multipliedArray = await MultiplyArrayAsync(array);
            var sortedArray = await SortArrayAsync(multipliedArray);
            var average = sortedArray.Average();

            Console.WriteLine("Original Array: " + String.Join(",", array));
            Console.WriteLine("Multiplied Array: " + String.Join(",", multipliedArray));
            Console.WriteLine("Sorted Array: " + String.Join(",", sortedArray));
            Console.WriteLine("Average: " + average);
            Console.ReadKey();
        }
    }
}
