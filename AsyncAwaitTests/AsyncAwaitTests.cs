using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using AsyncAwaitTasks;

namespace AsyncAwaitTasksTests
{
    public class TaskChainingTest
    {
        [SetUp]
        public void Setup()
        {
            AsyncAwait.Seed = 228;
        }

        [Test]
        public async Task CreateArrayAsync()
        {
            int[] expected = new int[] { 8, 7, 2, 3, 2, 8, 1, 8, 2, 4 };
            int[] actual = await AsyncAwait.CreateArrayAsync();
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public async Task MultiplyArrayAsync()
        {
            int[] sourceArray = new int[] { 1, 2, 3, 4, 5 };
            int[] expected = new int[] { 8, 16, 24, 32, 40 };
            int[] actual = await AsyncAwait.MultiplyArrayAsync(sourceArray);
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public async Task SortArrayAsync()
        {
            int[] sourceArray = new int[] { 5, 3, 2, 1, 4 };
            int[] expected = new int[] { 1, 2, 3, 4, 5 };
            int[] actual = await AsyncAwait.SortArrayAsync(sourceArray);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void MultiplyArrayAsync_ThrowsArgumentException_WhenArrayIsEmpty()
        {
            Assert.Throws<ArgumentException>(() => AsyncAwait.MultiplyArrayAsync(Array.Empty<int>()));
        }

        [Test]
        public void SortArrayAsync_ThrowsArgumentException_WhenArrayIsEmpty()
        {
            Assert.Throws<ArgumentException>(() => AsyncAwait.MultiplyArrayAsync(Array.Empty<int>()));
        }
    }
}